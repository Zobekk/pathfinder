#!/bin/sh

APP_JAR=$(ls pathfinder-*.jar)
echo "APP_JAR = ${APP_JAR}"

java -jar "${APP_JAR}"
