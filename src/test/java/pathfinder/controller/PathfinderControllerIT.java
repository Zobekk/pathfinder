package pathfinder.controller;

import pathfinder.model.Points;
import pathfinder.model.request.PathfinderRequest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class PathfinderControllerIT {

    @Autowired
    private PathfinderController pathfinderController;

    @Test
    void test() {
        PathfinderRequest request = new PathfinderRequest();
        request.setPoints(new ArrayList<>());

        request.getPoints().add(Points.A);
        request.getPoints().add(Points.B);
        request.getPoints().add(Points.C);

        assertEquals(12, pathfinderController.pathfinder(request).getDistance());
    }

    @Test
    void testHello() {
        assertEquals("Greetings !", pathfinderController.hello());
    }
}