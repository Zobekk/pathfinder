package pathfinder.controller;

import pathfinder.mapper.request.PathfinderCriteriaMapper;
import pathfinder.model.PathfinderCriteria;
import pathfinder.model.PathfinderCriterias;
import pathfinder.model.Paths;
import pathfinder.model.request.PathfinderRequest;
import pathfinder.model.response.Path;
import pathfinder.service.PathfinderService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class PathfinderControllerTest {

    @Mock
    private PathfinderService pathfinderService;
    @Mock
    private PathfinderCriteriaMapper pathfinderCriteriaMapper;

    @InjectMocks
    private PathfinderController pathfinderController;

    @Mock
    private PathfinderRequest pathfinderRequest;
    private final PathfinderCriteria pathfinderCriteria = PathfinderCriterias.THREE_POINTS;
    private final Path path = Paths.THREE_POINTS;

    @BeforeEach
    void setup() {
        doReturn(pathfinderCriteria).when(pathfinderCriteriaMapper).map(pathfinderRequest);
        doReturn(path).when(pathfinderService).findShortestPath(pathfinderCriteria);
    }

    @Test
    void test() {
        assertEquals(path, pathfinderController.pathfinder(pathfinderRequest));
    }
}