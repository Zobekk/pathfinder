package pathfinder.controller;

import pathfinder.model.Point;
import pathfinder.model.Points;
import pathfinder.model.exception.InvalidRequestException;
import pathfinder.model.exception.PointNotFoundException;
import pathfinder.storage.PointStorage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PointsControllerIT {

    @Autowired
    private PointsController pointsController;

    @Autowired
    private PointStorage pointStorage;

    @BeforeEach
    void cleanStorage() {
        pointStorage.deleteAllPoints();
    }

    @Nested
    class addPoints {

        @Test
        @DisplayName("Saves a list of points.")
        void addPoint() {
            List<Point> requestPoints = List.of(Points.PARIS, Points.LONDON);
            List<Point> responsePoints = pointsController.addPoints(requestPoints);
            assertEquals(requestPoints, responsePoints);
        }

        @Test
        @DisplayName("Error case: Adding a point without an Id")
        void addPointWithoutId() {
            Point unnamedPoint = Points.A;
            assertThrows(InvalidRequestException.class, () -> pointsController.addPoints(List.of(unnamedPoint)));
        }
    }

    @Nested
    class getPoints {

        @Test
        @DisplayName("Retrieve a single point.")
        void getPoint() {
            Point testPoint = Points.TOKYO;

            pointStorage.savePoint(testPoint);

            assertEquals(testPoint, pointsController.getPoint(testPoint.getId()));
        }

        @Test
        @DisplayName("Error case: Point does not exist.")
        void invalidPointId() {
            assertThrows(PointNotFoundException.class, () -> pointsController.getPoint("Invalid id"));
        }

        @Test
        @DisplayName("Error case: null point Id.")
        void nullPointId() {
            assertThrows(InvalidRequestException.class, () -> pointsController.getPoint(null));
            assertThrows(InvalidRequestException.class, () -> pointsController.getPoint(" "));
        }
    }

    @Nested
    class getAllPoints {

        @Test
        @DisplayName("Retrieve all stored points.")
        void getAllStoredPoints() {
            pointStorage.savePoint(Points.PARIS);
            pointStorage.savePoint(Points.LONDON);

            List<Point> retrievedPoints = pointsController.getAllPoints();

            assertEquals(2, retrievedPoints.size());
            assertTrue(retrievedPoints.containsAll(List.of(Points.PARIS, Points.LONDON)));
        }

        @Test
        @DisplayName("Return an empty list when no stored points.")
        void getAllWhenNoStoredPoint() {
            assertEquals(0, pointsController.getAllPoints().size());
        }
    }

    @Nested
    class deleteAllPoints {

        @Test
        @DisplayName("Delete All Points")
        void testDeleteAllPoints() {
            pointStorage.savePoint(Points.PARIS);
            pointStorage.savePoint(Points.LONDON);

            pointsController.deleteAllPoints();

            assertEquals(0, pointsController.getAllPoints().size());
        }
    }
}