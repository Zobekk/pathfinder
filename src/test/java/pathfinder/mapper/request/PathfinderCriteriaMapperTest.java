package pathfinder.mapper.request;

import pathfinder.model.PathfinderCriteria;
import pathfinder.model.Point;
import pathfinder.model.Points;
import pathfinder.model.request.PathfinderRequest;
import pathfinder.service.DistanceService;
import pathfinder.storage.PointStorage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class PathfinderCriteriaMapperTest {

    private static final double[][] DISTANCES = {{0}};

    @Mock
    private PointStorage pointStorage;

    @Mock
    private DistanceService distanceService;

    @InjectMocks
    private PathfinderCriteriaMapper pathfinderCriteriaMapper;

    @BeforeEach
    void setup() {
        doReturn(DISTANCES).when(distanceService).computeDistances(anyList());
    }

    @Test
    @DisplayName("Builds simple criteria for unnamed points")
    void testUnnamedPointsMapping() {
        // Given
        PathfinderRequest request = new PathfinderRequest();
        List<Point> points = List.of(Points.A, Points.B, Points.C);
        request.setPoints(points);

        // When
        PathfinderCriteria criteria = pathfinderCriteriaMapper.map(request);

        // Then
        assertEquals(DISTANCES, criteria.getDistance());
        assertEquals(points, criteria.getPoints());
    }

    @Test
    @DisplayName("Builds simple criteria for named points")
    void testNamedPointsMapping() {
        // Given
        PathfinderRequest request = new PathfinderRequest();
        List<Point> points = List.of(
            new Point("Paris", 0, 0),
            new Point("London", 0, 0));
        request.setPoints(points);
        doReturn(Points.PARIS).when(pointStorage).getPoint(eq("Paris"));
        doReturn(Points.LONDON).when(pointStorage).getPoint(eq("London"));

        // When
        PathfinderCriteria criteria = pathfinderCriteriaMapper.map(request);

        // Then
        assertEquals(DISTANCES, criteria.getDistance());
        assertEquals(2, criteria.getPoints().size());
        assertEquals(Points.PARIS, criteria.getPoints().get(0));
        assertEquals(Points.LONDON, criteria.getPoints().get(1));
    }
}