package pathfinder.service;

import pathfinder.model.PathfinderCriteria;
import pathfinder.model.PathfinderCriterias;
import pathfinder.model.response.Path;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PathfinderServiceTest {

    private final PathfinderService pathfinderService = new PathfinderService();

    private static Stream<Arguments> testCases() {
        return Stream.of(
            Arguments.of("One point", PathfinderCriterias.ONE_POINT, 0),
            Arguments.of("Two points", PathfinderCriterias.TWO_POINTS, 6),
            Arguments.of("Three points", PathfinderCriterias.THREE_POINTS, 12));
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("testCases")
    @DisplayName("Find the best path for trivial cases")
    void findPathForTrivialCases(String testName, PathfinderCriteria criteria, double expectedDistance) {
        Path path = pathfinderService.findShortestPath(criteria);
        assertEquals(expectedDistance, path.getDistance());
        assertEquals(criteria.getPoints(), path.getPoints());
    }
}