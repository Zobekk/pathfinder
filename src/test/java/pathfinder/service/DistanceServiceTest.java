package pathfinder.service;

import pathfinder.model.Point;
import pathfinder.model.Points;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DistanceServiceTest {

    private final DistanceService distanceService = new DistanceService();

    @Test
    @DisplayName("Builds the distance matrix for three points.")
    void testBasicMapping() {
        // Given
        List<Point> points = List.of(Points.A, Points.B, Points.C);

        // When
        double[][] result = distanceService.computeDistances(points);

        // Then
        double[][] expectedResult = {
            {0, 3, 4},
            {0, 0, 5},
            {0, 0, 0}
        };
        assertTrue(Arrays.deepEquals(expectedResult, result));
    }
}