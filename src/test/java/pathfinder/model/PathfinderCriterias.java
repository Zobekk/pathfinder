package pathfinder.model;

import java.util.List;

/**
 * Set of pre-built {@link PathfinderCriteria}s for test purposes.
 */
public class PathfinderCriterias {

    private static final double[][] ONE_POINT_DISTANCES = {{0}};
    private static final double[][] TWO_POINT_DISTANCES = {
        {0, 3},
        {0, 0}
    };
    private static final double[][] THREE_POINT_DISTANCES = {
        {0, 3, 4},
        {0, 0, 5},
        {0, 0, 0}
    };

    public static final PathfinderCriteria ONE_POINT = buildCriteria(List.of(Points.A), ONE_POINT_DISTANCES);
    public static final PathfinderCriteria TWO_POINTS = buildCriteria(List.of(Points.A, Points.B), TWO_POINT_DISTANCES);
    public static final PathfinderCriteria THREE_POINTS = buildCriteria(List.of(Points.A, Points.B, Points.C), THREE_POINT_DISTANCES);

    private static PathfinderCriteria buildCriteria(List<Point> points, double[][] distances) {
        return PathfinderCriteria.builder()
            .points(points)
            .distance(distances)
            .build();
    }
}
