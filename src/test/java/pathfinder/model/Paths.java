package pathfinder.model;

import pathfinder.model.response.Path;

import java.util.List;

public class Paths {

    public static final Path THREE_POINTS = Path.builder()
        .distance(12)
        .points(List.of(Points.A, Points.B, Points.C))
        .build();
}
