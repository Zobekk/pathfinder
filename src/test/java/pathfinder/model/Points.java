package pathfinder.model;

/**
 * A set of pre-built {@link Point}s for test purposes
 */
public class Points {

    public static final Point A = new Point(null, 0, 0);
    public static final Point B = new Point(null, 0, 3);
    public static final Point C = new Point(null, 4, 0);

    public static final Point PARIS = new Point("Paris", 0, 0);
    public static final Point LONDON = new Point("London", 0, 3);
    public static final Point TOKYO = new Point("Tokyo", 4, 0);
}
