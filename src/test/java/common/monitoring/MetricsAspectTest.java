package common.monitoring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MetricsAspectTest {

    @Mock
    private BasicLogService basicLogService;

    @InjectMocks
    private MetricsAspect metricsAspect;

    @Mock
    private Metrics metrics;

    @Mock
    private ProceedingJoinPoint joinPoint;

    @BeforeEach
    void setup() {
        Mockito.reset(basicLogService);
        doNothing().when(basicLogService).logInfo(anyString());
    }

    @Test
    @DisplayName("No error: Verify that the Aspect returns the method's result and logs something.")
    void testWhenNoException() throws Throwable {
        // Given
        Object expectedMethodResult = mock(Object.class);
        doReturn(expectedMethodResult).when(joinPoint).proceed();

        // When
        Object result = metricsAspect.logMetrics(joinPoint, metrics);

        // Then
        assertEquals(expectedMethodResult, result);
        verify(basicLogService).logInfo(anyString());
    }

    @Test
    @DisplayName("Error case: Verify that the Aspect rethrows the method's exception and logs something.")
    void testWhenException() throws Throwable {
        // Given
        doThrow(Exception.class).when(joinPoint).proceed();

        // When
        assertThrows(Exception.class, () -> metricsAspect.logMetrics(joinPoint, metrics));

        // then
        verify(basicLogService).logInfo(anyString());
    }
}