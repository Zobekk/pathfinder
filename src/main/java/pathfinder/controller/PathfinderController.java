package pathfinder.controller;

import common.monitoring.Metrics;
import lombok.RequiredArgsConstructor;
import pathfinder.mapper.request.PathfinderCriteriaMapper;
import pathfinder.model.PathfinderCriteria;
import pathfinder.model.Point;
import pathfinder.model.request.PathfinderRequest;
import pathfinder.model.response.Path;
import pathfinder.service.PathfinderService;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller providing the main method for computing a {@link Path} linking {@link Point}s
 */
@RestController
@RequestMapping(value = "/api")
@RequiredArgsConstructor
public class PathfinderController {

    private final PathfinderCriteriaMapper pathfinderCriteriaMapper;
    private final PathfinderService pathfinderService;

    /**
     * GET method building a {@link Path} from a {@link PathfinderRequest}.
     * 
     * @param request the {@link PathfinderRequest}
     * @return a {@link Path}
     */
    @Metrics(name = "pathfinder")
    @RequestMapping(value = "/pathfinder", consumes = "application/json", produces = "application/json")
    public Path pathfinder(@RequestBody PathfinderRequest request) {
        PathfinderCriteria criteria = pathfinderCriteriaMapper.map(request);
        return pathfinderService.findShortestPath(criteria);
    }

    @RequestMapping(value = "/healthcheck")
    public String hello() {
        return "Greetings !";
    }
}