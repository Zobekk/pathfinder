package pathfinder.controller;

import common.monitoring.Metrics;
import lombok.RequiredArgsConstructor;
import pathfinder.model.Point;
import pathfinder.model.exception.InvalidRequestException;
import pathfinder.model.exception.PointNotFoundException;
import pathfinder.storage.PointStorage;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller providing method to add/remove/get {@link Point}s in the database
 */
@RestController
@RequestMapping(value = "/api")
@RequiredArgsConstructor
public class PointsController {

    private final PointStorage pointStorage;

    /**
     * POST method for storing one or several {@link Point}s.
     * Overrides any existing point having the same Id.
     * 
     * @param pointsToSave the list of {@link Point}s to save.
     * @return the saved {@link Point} if successful.
     * @throws InvalidRequestException if the point to save has no Id.
     */
    @Metrics(name = "addPoints")
    @PostMapping(value = "/addPoints", consumes = "application/json", produces = "application/json")
    public List<Point> addPoints(@RequestBody List<Point> pointsToSave) {
        return pointsToSave.stream().map(pointStorage::savePoint).collect(Collectors.toList());
    }

    /**
     * GET method for retrieving a {@link Point} from its Id.
     * 
     * @param id the point Id
     * @return the {@link Point} if it exists.
     * @throws InvalidRequestException if the requested Id is null or empty.
     * @throws PointNotFoundException if the requested Id does not exist.
     */
    @Metrics(name = "getPoint")
    @GetMapping(value = "/getPoint", produces = "application/json")
    public Point getPoint(@RequestParam String id) {
        return pointStorage.getPoint(id);
    }

    /**
     * GET method for retrieving all stored {@link Point}s.
     *
     * @return a list of all stored {@link Point}s
     */
    @Metrics(name = "getAllPoints")
    @GetMapping(value = "/getAllPoints", produces = "application/json")
    public List<Point> getAllPoints() {
        return pointStorage.getAllPoints();
    }

    /**
     * DELETE method for deleting all stored {@link Point}s.
     */
    @Metrics(name = "deleteAllPoints")
    @DeleteMapping(value = "/deleteAllPoints")
    public void deleteAllPoints() {
        pointStorage.deleteAllPoints();
    }
}