package pathfinder.mapper.request;

import lombok.RequiredArgsConstructor;
import pathfinder.model.PathfinderCriteria;
import pathfinder.model.Point;
import pathfinder.model.exception.InvalidRequestException;
import pathfinder.model.request.PathfinderRequest;
import pathfinder.service.DistanceService;
import pathfinder.storage.PointStorage;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class PathfinderCriteriaMapper {

    private final DistanceService distanceService;
    private final PointStorage pointStorage;

    /**
     * Maps a {@link PathfinderRequest} to a {@link PathfinderCriteria}.
     * 
     * @param request the {@link PathfinderRequest} to map.
     * @return the mapped {@link PathfinderCriteria}.
     */
    public PathfinderCriteria map(PathfinderRequest request) {
        List<Point> mappedPoints = mapPoints(request.getPoints());
        double[][] distance = distanceService.computeDistances(mappedPoints);

        return PathfinderCriteria.builder()
            .points(mappedPoints)
            .distance(distance)
            .build();
    }

    /**
     * f the provided points have an id, retrieve the corresponding point from the storage.
     * Otherwise, keep the point as is.
     */
    private List<Point> mapPoints(List<Point> requestPoints) {
        if (CollectionUtils.isEmpty(requestPoints)) {
            throw new InvalidRequestException("At least one point is expected.");
        }

        List<Point> mappedPoints = new ArrayList<>();
        for (Point point : requestPoints) {
            if (point.getId() != null) {
                mappedPoints.add(pointStorage.getPoint(point.getId()));
            }
            else {
                mappedPoints.add(point);
            }
        }
        return mappedPoints;
    }
}
