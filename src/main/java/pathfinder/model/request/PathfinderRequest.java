package pathfinder.model.request;

import lombok.Data;
import pathfinder.model.Point;

import java.util.List;

@Data
public class PathfinderRequest {
    List<Point> points;
}
