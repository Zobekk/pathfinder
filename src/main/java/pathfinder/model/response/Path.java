package pathfinder.model.response;

import lombok.Builder;
import lombok.Value;
import pathfinder.model.Point;

import java.util.List;

@Value
@Builder
public class Path {
    double distance;
    List<Point> points;
}
