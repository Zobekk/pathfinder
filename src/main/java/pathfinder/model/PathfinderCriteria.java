package pathfinder.model;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class PathfinderCriteria {

    /**
     * The list of requested {@link Point}s
     */
    @NonNull
    List<Point> points;

    /**
     * A strictly upper triangular matrix containing the distance between each pair of requested {@link Point}, identified by their index.
     */
    double[][] distance;
}
