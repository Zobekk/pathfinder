package pathfinder.model.exception;

public abstract class AbstractFunctionalException extends RuntimeException {

    public AbstractFunctionalException(String message) {
        super(message);
    }
}
