package pathfinder.model.exception;

public class PointNotFoundException extends AbstractFunctionalException {

    public PointNotFoundException(String pointId) {
        super("Point id " + pointId + " not found.");
    }
}
