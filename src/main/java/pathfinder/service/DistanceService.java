package pathfinder.service;

import pathfinder.model.Point;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service computing the distance matrix from a list of points.
 */
@Service
public class DistanceService {

    /**
     * @param points a list of {@link Point}s. Should not be null or empty.
     * @return a strictly upper triangular matrix containing the distance between each pair of requested {@link Point}, identified by their index.
     */
    public double[][] computeDistances(List<Point> points) {

        int n = points.size();
        double[][] distance = new double[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                distance[i][j] = euclideanNorm(points.get(i), points.get(j));
            }
        }

        return distance;
    }

    /**
     * Return the euclidean norm between two {@link Point}s.
     */
    public static double euclideanNorm(Point point1, Point point2) {
        double dx = point1.getX() - point2.getX();
        double dy = point1.getY() - point2.getY();
        return Math.sqrt(dx * dx + dy * dy);
    }
}
