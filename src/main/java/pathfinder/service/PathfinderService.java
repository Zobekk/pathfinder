package pathfinder.service;

import pathfinder.model.PathfinderCriteria;
import pathfinder.model.Point;
import pathfinder.model.response.Path;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PathfinderService {

    /**
     * Return a {@link Path} object, corresponding to a path between all points.
     * Does the best effort: if an exact solution can easily be found, it will be returned.
     * Otherwise, returns a good solution.
     * 
     * @param criteria the required {@link PathfinderCriteria}.
     * @return a {@link Path}
     */
    public Path findShortestPath(PathfinderCriteria criteria) {
        List<Point> points = criteria.getPoints();

        if (points.size() <= 3) {
            return solveTrivialCase(criteria);
        }
        else {
            // TODO handle complex cases
            return Path.builder()
                .distance(42)
                .points(points)
                .build();
        }
    }

    /**
     * There is only one possible path linking three or less points. Returns it.
     * 
     * @param criteria the {@link PathfinderCriteria} containing three or less points.
     * @return the only possible {@link Path}.
     */
    private Path solveTrivialCase(PathfinderCriteria criteria) {
        List<Point> points = criteria.getPoints();
        Path.PathBuilder pathBuilder = Path.builder().points(points);

        if (points.size() == 3) {
            pathBuilder.distance(criteria.getDistance()[0][1] + criteria.getDistance()[0][2] + criteria.getDistance()[1][2]);
        }
        else if (points.size() == 2) {
            pathBuilder.distance(2 * criteria.getDistance()[0][1]);
        }
        else {
            pathBuilder.distance(0);
        }

        return pathBuilder.build();
    }
}
