package pathfinder.storage;

import pathfinder.model.Point;

import org.springframework.data.jpa.repository.JpaRepository;

interface PointRepository extends JpaRepository<Point, String> {}
