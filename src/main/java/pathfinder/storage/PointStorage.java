package pathfinder.storage;

import lombok.RequiredArgsConstructor;
import pathfinder.model.Point;
import pathfinder.model.exception.InvalidRequestException;
import pathfinder.model.exception.PointNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service providing method to store and retrieve {@link Point}s
 */
@Service
@RequiredArgsConstructor
public class PointStorage {

    private final PointRepository pointRepository;

    /**
     * Returns the desired {@link Point} from its id.
     * 
     * @param id the desired point id
     * @return the corresponding {@link Point} if it exists.
     * @throws InvalidRequestException if the id is not valid.
     * @throws PointNotFoundException if the id does not exist.
     */
    public Point getPoint(String id) {
        if (isNotValid(id)) {
            throw new InvalidRequestException("Point id is required.");
        }
        return pointRepository.findById(id)
            .orElseThrow(() -> new PointNotFoundException(id));
    }

    /**
     * @return the list of all stored {@link Point}s.
     */
    public List<Point> getAllPoints() {
        return pointRepository.findAll();
    }

    /**
     * Store a {@link Point}.
     * 
     * @param pointToSave the {@link Point} to store.
     * @return the stored {@link Point} if successful.
     * @throws InvalidRequestException if the point id is not valid.
     */
    public Point savePoint(Point pointToSave) {
        if (isNotValid(pointToSave.getId())) {
            throw new InvalidRequestException("Point id is required.");
        }
        return pointRepository.save(pointToSave);
    }

    /**
     * Deletes all stored {@link Point}s.
     */
    public void deleteAllPoints() {
        pointRepository.deleteAll();
    }

    private boolean isNotValid(String id) {
        return StringUtils.isEmpty(id) || StringUtils.isBlank(id);
    }
}
