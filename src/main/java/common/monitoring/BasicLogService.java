package common.monitoring;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

/**
 * Console logger.
 */
@Slf4j
@Component
class BasicLogService {

    void logInfo(String message) {
        log.info(message);
    }
}
