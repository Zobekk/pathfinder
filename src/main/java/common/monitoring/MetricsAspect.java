package common.monitoring;

import lombok.RequiredArgsConstructor;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * A Spring aspect to deal with methods annotated with {@link Metrics}.
 */
@Aspect
@Component
@RequiredArgsConstructor
public class MetricsAspect {

    private final BasicLogService basicLogService;

    /**
     * Compute the execution time of a method annotated with {@link Metrics} and log it.
     * 
     * @param joinPoint the {@link ProceedingJoinPoint}
     * @param metrics the {@link Metrics}
     * @return the result of the underlying method.
     * @throws Throwable the underlying method exception.
     */
    @Around("@annotation(metrics)")
    public Object logMetrics(ProceedingJoinPoint joinPoint, Metrics metrics) throws Throwable {
        long startTime = System.currentTimeMillis();

        Object result;
        try {
            result = joinPoint.proceed();
        }
        finally {
            long executionTime = System.currentTimeMillis() - startTime;
            basicLogService.logInfo(metrics.name() + " execution time: " + executionTime + " ms");
        }

        return result;
    }
}
