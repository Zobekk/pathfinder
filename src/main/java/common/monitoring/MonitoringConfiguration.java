package common.monitoring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Configuration class to enable the monitoring aspect.
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "common.monitoring")
public class MonitoringConfiguration {}
