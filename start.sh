# Run the application from scratch

mvn clean package -Dmaven.test.skip=true
sudo docker build -t pathfinder_image .
sudo docker stop pathfinder
sleep 1
sudo docker run --rm --name pathfinder -p 9024:9024 -d pathfinder_image
