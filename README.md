# Pathfinder API

This API's main goal is to find the shortest circuit linking any number of points only once (Travelling Salesman Problem).  

The following examples are in the provided Postman collection.  
The API can currently be accessed here: *15.236.142.161:9024* (ex: http://15.236.142.161:9024/api/healthcheck)

***
## Handling Points
This API presents operations for storing and reusing 2D points using a simple id.  
These points can then be reused for pathfinding.

### Adding points: POST operation on */api/addPoints*

Request body example:

    [
        {"id": "Paris", "x": 0, "y": 0},
        {"id": "London", "x": 3, "y": 0},
        {"id": "Tokyo", "x": 0, "y": 4},
    ]
This example stores three points for future usage.    

### Retrieving a point: GET operation on */api/getPoint?id=\<pointId\>*
This returns the point corresponding to the requested id (if it exists).
### Retrieving all points: GET operation on */api/getAllPoints*
This returns all the stored points.
### Deleting all points: DELETE operation on */api/deleteAllPoints*
This deletes all the stored points.

***
## Main operation: */api/pathfinder*
This operation requires a request body containing a list of 2D points.  
It returns the shortest distance as well as the ordered list of points representing the shortest route between all of them.

It works with simple coordinates:

    Request:
    {
        "points": [
            { "x": 0, "y": 0 },
            { "x": 3, "y": 0 },
            { "x": 0, "y": 4 }
        ]
    }
    
    Response:
    {
        "distance": 12.0,
        "points": [
            { "id": null, "x": 0.0, "y": 0.0 },
            { "id": null, "x": 3.0, "y": 0.0 },
            { "id": null, "x": 0.0, "y": 4.0 }
        ]
    }
    
    
Or with stored points:

    Request:
    {
        "points": [
            {"id": "Paris"},
            {"id": "London"},
            {"id": "Tokyo"}
        ]
    }
    
    Response:
    {
        "distance": 12.0,
        "points": [
            { "id": "Paris", "x": 0.0, "y": 0.0 },
            { "id": "London", "x": 3.0, "y": 0.0 },
            { "id": "Tokyo", "x": 0.0, "y": 4.0 }
        ]
    }
    

    
NB: This only works for 3 of less points for now (main algorithm to be implemented).
