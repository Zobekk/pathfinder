FROM openjdk:11

COPY target/pathfinder-*.jar ./
COPY entrypoint.sh ./

EXPOSE 9024

ENTRYPOINT ["bash", "./entrypoint.sh"]